package uk.sliske.runescape.util.quests;

import static uk.sliske.runescape.util.quests.SkillReq.*;
import static uk.sliske.runescape.util.quests.SkillReq.SKILL.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uk.sliske.runescape.util.quests.OtherReq.REQS;
import uk.sliske.runescape.util.quests.SkillReq.SKILL;
import uk.sliske.runescape.util.quests.list.elf.*;
import uk.sliske.runescape.util.quests.list.fairy.FairyTale1;
import uk.sliske.runescape.util.quests.list.mahjarrat.*;
import uk.sliske.runescape.util.quests.list.rfd.*;

public class Quest extends Requirement {

	private List<Quest> questReqs;
	private List<SkillReq> skillReqs;
	public final String name;
	private HashMap<SKILL, Integer> xpRewards;
	private List<SkillReq> skillsAtEnd;

	public Quest(String name, Requirement... reqs) {
		super(TYPE.QUEST);
		this.name = name;
		init(reqs);
	}

	public Quest(String name, Requirement[] reqs1, Requirement... reqs2) {
		super(TYPE.QUEST);
		this.name = name;
		List<Requirement> reqs = new ArrayList<>();
		for (Requirement r : reqs1) {
			reqs.add(r);
		}
		for (Requirement r : reqs2) {
			reqs.add(r);
		}
		Requirement[] rs = new Requirement[reqs.size()];
		reqs.toArray(rs);
		init(rs);

	}
	protected final void addXpReward(SKILL skill, int amt){
		xpRewards.put(skill, amt);
	}

	private final void calcCompletedStats() {
		skillsAtEnd = new ArrayList<>();
		skillsAtEnd.addAll(skillReqs);
		// for(SKILL)
	}

	private final void init(Requirement... reqs) {
		questReqs = new ArrayList<>();
		skillReqs = new ArrayList<>();
		xpRewards = new HashMap<>();
		for (Requirement r : reqs) {
			switch (r.type) {
			case SKILL:
				skillReqs.add((SkillReq) r);
				break;
			case QUEST:
				questReqs.add((Quest) r);
				break;
			}

		}
	}

	public HashMap<SKILL, Skill> xpWhenComplete(HashMap<SKILL, Skill> xps) {
		xps = new HashMap<>();
		for (Quest q : questReqs) {
			HashMap<SKILL, Skill> hms = q.xpWhenComplete();
			for (SKILL s : hms.keySet()) {
				if (!xps.containsKey(s)) {
					xps.put(s, hms.get(s));
				}
				for (SkillReq sq : skillReqs) {
					if (sq.skill == s) {
						int xp = max(xps.get(s).xp(), sq.xp(), hms.get(s).xp());
						xps.put(s, new Skill(xp, true));
					}
				}
			}
		}
		for (SkillReq sq : skillReqs) {
			int xp = 0;
			if (xps.containsKey(sq.skill))
				xp = max(xps.get(sq.skill).xp(), sq.xp());
			else
				xp = sq.xp();
			xps.put(sq.skill, new Skill(xp, true));
		}
		for (SKILL s : xpRewards.keySet()) {
			if (!xps.containsKey(s)) {
				xps.put(s, new Skill(xpRewards.get(s), true));
			} else {
				xps.get(s).addXP(xpRewards.get(s));
			}
		}

		return xps;
	}
	public HashMap<SKILL, Skill> xpWhenComplete(){
		return xpWhenComplete(new HashMap<>());
	}
	public static int max(int i, int... args) {
		for (int a : args) {
			if (a > i)
				return max(a, args);
		}
		return i;

	}

	public List<SkillReq> getFlattenedSkillReqsList() {
		HashMap<SKILL, Integer> skills = new HashMap<>();
		// System.out.println();
		for (Quest q : getFlattenedQuestReqsList()) {
			for (SkillReq s : q.getSkillReqs()) {
				if (skills.containsKey(s.skill)) {
					if (s.level() > skills.get(s.skill)) {
						skills.put(s.skill, s.level());
					}
				} else {
					skills.put(s.skill, s.level());
				}
			}
		}

		List<SkillReq> res = new ArrayList<>();
		for (SKILL s : skills.keySet()) {
			res.add(new SkillReq(s, skills.get(s)));
		}
		return res;
	}

	public List<Quest> getFlattenedQuestReqsList() {
		List<Quest> res = new ArrayList<>();
		res.addAll(getFlattenedQuestReqsSet());
		res.remove(this);
		return res;
	}

	public Set<Quest> getFlattenedQuestReqsSet() {
		Set<Quest> reqs = new HashSet<>();
		// System.out.println(this.name);
		reqs.add(this);
		for (Quest q : questReqs) {
			reqs.addAll(q.getFlattenedQuestReqsSet());
		}
		return reqs;
	}

	public List<SkillReq> getSkillReqs() {
		return skillReqs;
	}

	public int getSkillReq(SKILL skill) {
		for (SkillReq s : skillReqs) {
			if (s.skill == skill) {
				return s.level();
			}
		}
		return 0;
	}

	private static SkillReq skill(SKILL skill, int lvl) {
		return new SkillReq(skill, lvl);
	}

	private static SkillReq[] skills(int lvl, SKILL... skills) {
		int l = skills.length;
		SkillReq[] res = new SkillReq[l];
		for (int i = 0; i < l; i++) {
			res[i] = skill(skills[i], lvl);
		}
		return res;
	}

	// OS
	public static final Quest ROMEO_AND_JULIET = new Quest("ROMEO_AND_JULIET");
	public static final Quest SHEEP_SHEARER = new Quest("SHEEP_SHEARER");
	public static final Quest PRINCE_ALI_RESCUE = new Quest("PRINCE_ALI_RESCUE"); /* 10 */
	public static final Quest DORICS_QUEST = new Quest("DORICS_QUEST");
	public static final Quest BLACK_KNIGHTS_FORTRESS = new Quest(
			"BLACK_KNIGHTS_FORTRESS");

	// F2P
	public static final Quest SHADOW_OVER_ASHDALE = new Quest(
			"SHADOW_OVER_ASHDALE");
	public static final Quest BLOOD_PACT = new Quest("BLOOD_PACT");
	public static final Quest COOKS_ASSISTANT = new Quest("Cooks Assistant");
	public static final Quest DEATH_OF_CHIVALRY = new Quest("DEATH_OF_CHIVALRY");
	public static final Quest DEATH_PLATEAU = new Quest("DEATH_PLATEAU");
	public static final Quest DEMON_SLAYER = new Quest("DEMON_SLAYER");
	public static final Quest ERNEST_THE_CHICKEN = new Quest(
			"ERNEST_THE_CHICKEN");
	public static final Quest GOBLIN_DIPLOMACY = new Quest("GOBLIN_DIPLOMACY");
	public static final Quest GUNNARS_GROUND = new Quest("GUNNARS_GROUND",
			skill(CRAFTING, 5));
	public static final Quest IMP_CATCHER = new Quest("IMP_CATCHER");
	public static final Quest LET_THEM_EAT_PIE = new Quest("LET_THEM_EAT_PIE");
	public static final Quest MYTHS_OF_WHITE_LANDS = new Quest(
			"MYTHS_OF_WHITE_LANDS");
	public static final Quest PIRATES_TREASURE = new Quest("PIRATES_TREASURE");
	public static final Quest RESTLESS_GHOST = new Quest("RESTLESS_GHOST");
	public static final Quest RUNE_MYSTERIES = new Quest("RUNE_MYSTERIES");
	public static final Quest SHIELD_OF_ARRAV = new Quest("SHIELD_OF_ARRAV");
	public static final Quest STOLEN_HEARTS = new Quest("STOLEN_HEARTS");
	public static final Quest SWEPT_AWAY = new Quest("SWEPT_AWAY");
	public static final Quest VAMPYRE_SLAYER = new Quest("VAMPYRE_SLAYER");
	public static final Quest WHATS_MINE_IS_YOURS = new Quest(
			"WHATS_MINE_IS_YOURS", smithing(5));
	public static final Quest KNIGHTS_SWORD = new Quest("KNIGHTS_SWORD", skill(
			MINING, 10));
	public static final Quest DRAGON_SLAYER = new Quest("Dragon Slayer",
			questPoints(33));

	// P2P
	public static final Quest RFD_START = new Quest("RFD_START", cooking(10),
			COOKS_ASSISTANT);
	public static final Quest PLAGUE_CITY = new Quest("PLAGUE_CITY");
	public static final Quest BIOHAZARD = new Quest("BIOHAZARD", PLAGUE_CITY);
	public static final Quest BRINGING_HOME_THE_BACON = new Quest(
			"BRINGING_HOME_THE_BACON", skill(FARMING, 14),
			skill(SUMMONING, 14), skill(CONSTRUCTION, 14));
	public static final Quest BROKEN_HOME = new Quest("BROKEN_HOME");
	public static final Quest BUYERS_AND_CELLARS = new Quest(
			"BUYERS_AND_CELLARS", skill(THIEVING, 5));
	public static final Quest CLOCK_TOWER = new Quest("CLOCK_TOWER");
	public static final Quest DIAMOND_IN_THE_ROUGH = new Quest(
			"DIAMOND_IN_THE_ROUGH", STOLEN_HEARTS);
	public static final Quest DRUIDIC_RITUAL = new Quest("DRUIDIC_RITUAL");
	public static final Quest DWARF_CANNON = new Quest("DWARF_CANNON");
	public static final Quest EAGLES_PEAK = new Quest("EAGLES_PEAK", skill(
			HUNTER, 27));
	public static final Quest ELEMENTAL_WORKSHOP_1 = new Quest(
			"ELEMENTAL_WORKSHOP_1", skill(MINING, 20), smithing(20), skill(
					CRAFTING, 20));
	public static final Quest FISHING_CONTEST = new Quest("FISHING_CONTEST",
			skill(FISHING, 10));
	public static final Quest RAG_AND_BONE_MAN = new Quest("RAG_AND_BONE_MAN");
	public static final Quest FUR_N_SEEK = new Quest("FUR_N_SEEK", skill(
			SLAYER, 25), RAG_AND_BONE_MAN);
	public static final Quest GERTUDES_CAT = new Quest("GERTUDES_CAT");
	public static final Quest HAZEEL_CULT = new Quest("HAZEEL_CULT");
	public static final Quest JUNGLE_POTION = new Quest("JUNGLE_POTION", skill(
			HERBLORE, 3), DRUIDIC_RITUAL);
	public static final Quest MISSING_PRESUMED_DEATH = new Quest(
			"MISSING_PRESUMED_DEATH");
	public static final Quest MONKS_FRIEND = new Quest("MONKS_FRIEND");
	public static final Quest MURDER_MYSTERY = new Quest("MURDER_MYSTERY");
	public static final Quest PRIEST_IN_PERIL = new Quest("PRIEST_IN_PERIL",
			RUNE_MYSTERIES);
	public static final Quest NATURE_SPIRIT = new Quest("NATURE_SPIRIT", skill(
			CRAFTING, 18), skill(PRAYER, 10), RESTLESS_GHOST, PRIEST_IN_PERIL);
	public static final Quest OBSERVATORY = new Quest("OBSERVATORY", skill(
			CRAFTING, 10));
	public static final Quest ONE_PIERCING_NOTE = new Quest("ONE_PIERCING_NOTE");
	public static final Quest PERILS_OF_ICE_MOUNTAIN = new Quest(
			"PERILS_OF_ICE_MOUNTAIN", skill(CONSTRUCTION, 10), skill(FARMING,
					10), skill(HUNTER, 10), skill(THIEVING, 11));
	public static final Quest RECRUITMENT_DRIVE = new Quest(
			"RECRUITMENT_DRIVE", BLACK_KNIGHTS_FORTRESS, DRUIDIC_RITUAL);
	public static final Quest WOLF_WHISTLE = new Quest("WOLF_WHISTLE");
	public static final Quest RUNE_MECHANICS = new Quest("RUNE_MECHANICS",
			skill(MAGIC, 27), skill(RUNECRAFTING, 20), skill(CONSTRUCTION, 25),
			RUNE_MYSTERIES, WOLF_WHISTLE);
	public static final Quest RUNE_MEMORIES = new Quest("RUNE_MEMORIES",
			RUNE_MYSTERIES);
	public static final Quest SHEEP_HERDER = new Quest("SHEEP_HERDER");
	public static final Quest SONG_FROM_THE_DEPTHS = new Quest(
			"SONG_FROM_THE_DEPTHS");
	public static final Quest A_SOULS_BANE = new Quest("A_SOULS_BANE");
	public static final Quest TALE_OF_THE_MUSPAH = new Quest(
			"TALE_OF_THE_MUSPAH", skill(FIREMAKING, 6), skill(MINING, 8),
			skill(MAGIC, 10), skill(WOODCUTTING, 10));
	public static final Quest TOWER_OF_LIFE = new Quest("TOWER_OF_LIFE", skill(
			CONSTRUCTION, 10));
	public static final Quest ANIMAL_MAGNETISM = new Quest("ANIMAL_MAGNETISM",
			skill(SLAYER, 18), skill(CRAFTING, 19), skill(RANGED, 30), skill(
					WOODCUTTING, 35), RESTLESS_GHOST, ERNEST_THE_CHICKEN,
			PRIEST_IN_PERIL);
	public static final Quest CARNILLEAN_RISING = new Quest(
			"CARNILLEAN_RISING", skill(THIEVING, 33), skill(CONSTRUCTION, 31),
			questPoints(50), HAZEEL_CULT, BLOOD_PACT);
	public static final Quest CREATURE_OF_FENKENSTRAIN = new Quest(
			"CREATURE_OF_FENKENSTRAIN", skill(CRAFTING, 20),
			skill(THIEVING, 25), PRIEST_IN_PERIL, RESTLESS_GHOST);
	public static final Quest COLD_WAR = new Quest("COLD_WAR",
			skill(HUNTER, 10), skill(AGILITY, 30), skill(CRAFTING, 30), skill(
					CONSTRUCTION, 34));
	public static final Quest DIGSITE = new Quest("DIGSITE",
			skill(AGILITY, 10), skill(HERBLORE, 10), skill(THIEVING, 25));
	public static final Quest ENLIGHTENED_JOURNEY = new Quest(
			"ENLIGHTENED_JOURNEY", skill(FIREMAKING, 20), skill(FARMING, 30),
			skill(CRAFTING, 36), questPoints(20));
	public static final Quest THE_FUED = new Quest("THE_FUED", skill(THIEVING,
			30));
	public static final Quest GARDEN_OF_TRANQUILITY = new Quest(
			"GARDEN_OF_TRANQUILITY", skill(FARMING, 25),
			CREATURE_OF_FENKENSTRAIN);
	public static final Quest GHOSTS_AHOY = new Quest("GHOSTS_AHOY", skill(
			AGILITY, 25), skill(COOKING, 20), PRIEST_IN_PERIL, RESTLESS_GHOST);
	public static final Quest THE_GOLEM = new Quest("THE_GOLEM", skill(
			CRAFTING, 20), skill(THIEVING, 25));
	public static final Quest HAND_IN_THE_SAND = new Quest("HAND_IN_THE_SAND",
			skill(THIEVING, 17), skill(CRAFTING, 49));
	public static final Quest THE_LOST_TRIBE = new Quest("THE_LOST_TRIBE",
			skill(AGILITY, 13), skill(MINING, 17), skill(THIEVING, 13),
			GOBLIN_DIPLOMACY);
	public static final Quest WANTED = new Quest("WANTED", questPoints(33),
			RECRUITMENT_DRIVE, THE_LOST_TRIBE, PRIEST_IN_PERIL);
	public static final Quest QUIET_BEFORE_THE_SWARM = new Quest(
			"QUIET_BEFORE_THE_SWARM", skill(STRENGTH, 42), skill(ATTACK, 35),
			IMP_CATCHER, WANTED);
	public static final Quest ICTHLARINS_LITTLE_HELPER = new Quest(
			"ICTHLARINS_LITTLE_HELPER", DIAMOND_IN_THE_ROUGH, GERTUDES_CAT,
			RESTLESS_GHOST);
	public static final Quest RAT_CATCHERS = new Quest("RAT_CATCHERS",
			ICTHLARINS_LITTLE_HELPER);
	public static final Quest SEA_SLUG = new Quest("SEA_SLUG", skill(
			FIREMAKING, 30));
	public static final Quest THE_SLUG_MENACE = new Quest("THE_SLUG_MENACE",
			skills(30, CRAFTING, RUNECRAFTING, SLAYER, THIEVING), SEA_SLUG,
			WANTED);
	public static final Quest KENNITHS_CONCERNS = new Quest(
			"KENNITHS_CONCERNS", skill(MINING, 46), THE_SLUG_MENACE);
	public static final Quest SALT_IN_THE_WOUND = new Quest(
			"SALT_IN_THE_WOUND", skill(DEFENCE, 60), skill(HITPOINTS, 50),
			skill(HERBLORE, 47), skill(SUMMONING, 45),
			skill(DUNGEONEERING, 35), KENNITHS_CONCERNS);
	public static final Quest SCORPIAN_CATCHER = new Quest("SCORPIAN_CATCHER",
			skill(PRAYER, 31));
	public static final Quest SHADES_OF_MORTON = new Quest("SHADES_OF_MORTON",
			skill(CRAFTING, 20), skill(HERBLORE, 15), PRIEST_IN_PERIL);
	public static final Quest SHADOW_OF_THE_STORM = new Quest(
			"SHADOW_OF_THE_STORM", skill(CRAFTING, 30), DEMON_SLAYER, THE_GOLEM);
	public static final Quest SMOKING_KILLS = new Quest("SMOKING_KILLS", skill(
			CRAFTING, 25), skill(SLAYER, 35), combat(90), RESTLESS_GHOST,
			ICTHLARINS_LITTLE_HELPER);
	public static final Quest SPIRITS_OF_THE_ELID = new Quest(
			"SPIRITS_OF_THE_ELID", skill(MAGIC, 33), skill(RANGED, 37), skill(
					MINING, 37), skill(THIEVING, 37));
	public static final Quest A_TAIL_OF_TWO_CATS = new Quest(
			"A_TAIL_OF_TWO_CATS", ICTHLARINS_LITTLE_HELPER);
	public static final Quest TEARS_OF_GUTHIX = new Quest("TEARS_OF_GUTHIX",
			questPoints(43), skill(FIREMAKING, 49), skill(CRAFTING, 20), skill(
					MINING, 20));
	public static final Quest THE_TOURIST_TRAP = new Quest("THE_TOURIST_TRAP",
			skill(FLETCHING, 10), smithing(20));
	public static final Quest WATCHTOWER = new Quest("WATCHTOWER", skill(
			HERBLORE, 14), skill(MAGIC, 14), skill(THIEVING, 15), skill(
			AGILITY, 25), skill(MINING, 40));
	public static final Quest WATERFALL = new Quest("WATERFALL");
	public static final Quest WHAT_LIES_BELOW = new Quest("WHAT_LIES_BELOW",
			skill(RUNECRAFTING, 35), RUNE_MYSTERIES);
	public static final Quest WITCHS_HOUSE = new Quest("WITCHS_HOUSE");
	public static final Quest ALL_FIRED_UP = new Quest("ALL_FIRED_UP", skill(
			FIREMAKING, 43), PRIEST_IN_PERIL);
	public static final Quest MERLINS_CRYSTAL = new Quest("MERLINS_CRYSTAL");
	public static final Quest HOLY_GRAIL = new Quest("HOLY_GRAIL", skill(
			ATTACK, 20), MERLINS_CRYSTAL);
	public static final Quest DEATH_TO_THE_DORGESHUUN = new Quest(
			"DEATH_TO_THE_DORGESHUUN", skill(AGILITY, 23), skill(THIEVING, 23),
			THE_LOST_TRIBE);
	public static final Quest THE_GIANT_DWARF = new Quest("THE_GIANT_DWARF",
			skill(CRAFTING, 12), skill(FIREMAKING, 16), skill(MAGIC, 33),
			skill(THIEVING, 14));
	public static final Quest ANOTHER_SLICE_OF_HAM = new Quest(
			"ANOTHER_SLICE_OF_HAM", skill(ATTACK, 15), skill(PRAYER, 25),
			DEATH_TO_THE_DORGESHUUN, THE_GIANT_DWARF, DIGSITE);
	public static final Quest ELEMENTAL_WORKSHOP_2 = new Quest(
			"ELEMENTAL_WORKSHOP_2", skill(MAGIC, 20), smithing(30),
			ELEMENTAL_WORKSHOP_1);
	public static final Quest ELEMENTAL_WORKSHOP_3 = new Quest(
			"ELEMENTAL_WORKSHOP_3", skill(MINING, 20), skill(DEFENCE, 33),
			smithing(33), ELEMENTAL_WORKSHOP_2);
	public static final Quest ELEMENTAL_WORKSHOP_4 = new Quest(
			"ELEMENTAL_WORKSHOP_4", skill(CRAFTING, 41),
			skill(RUNECRAFTING, 39), skill(THIEVING, 39), skill(DEFENCE, 40),
			smithing(42), ELEMENTAL_WORKSHOP_3);
	public static final Quest THE_FREMMENIK_TRIALS = new Quest(
			"THE_FREMMENIK_TRIALS", skill(WOODCUTTING, 40),
			skill(CRAFTING, 40), skill(FLETCHING, 25));
	public static final Quest MOUNTAIN_DAUGHTER = new Quest(
			"MOUNTAIN_DAUGHTER", skill(AGILITY, 20));
	public static final Quest OLAFS_QUEST = new Quest("OLAFS_QUEST", skill(
			FIREMAKING, 40), skill(WOODCUTTING, 50), THE_FREMMENIK_TRIALS);
	public static final Quest TREE_GNOME_VILLAGE = new Quest(
			"TREE_GNOME_VILLAGE");
	public static final Quest THE_GRAND_TREE = new Quest("THE_GRAND_TREE",
			agility(25));
	public static final Quest THE_EYES_OF_GLOUPHRIE = new Quest(
			"THE_EYES_OF_GLOUPHRIE", THE_GRAND_TREE);
	public static final Quest MAKING_HISTORY = new Quest("MAKING_HISTORY",
			skill(MAGIC, 7), skill(CRAFTING, 20), smithing(40),
			PRIEST_IN_PERIL, DRUIDIC_RITUAL);
	public static final Quest TRIBAL_TOTEM = new Quest("TRIBAL_TOTEM", skill(
			THIEVING, 21));
	public static final Quest TAI_BWO_WANNAI_TRIO = new Quest(
			"TAI_BWO_WANNAI_TRIO", skill(AGILITY, 15), skill(COOKING, 30),
			skill(FISHING, 5), JUNGLE_POTION);
	public static final Quest MISSING_MY_MUMMY = new Quest("MISSING_MY_MUMMY",
			skills(35, CONSTRUCTION, COOKING, CRAFTING, MAGIC, PRAYER),
			PRINCE_ALI_RESCUE, THE_GOLEM, GARDEN_OF_TRANQUILITY,
			ICTHLARINS_LITTLE_HELPER);
	public static final Quest IN_SEARCH_OF_THE_MYREQUE = new Quest(
			"IN_SEARCH_OF_THE_MYREQUE", skill(AGILITY, 25), NATURE_SPIRIT);
	public static final Quest IN_AID_OF_THE_MYREQUE = new Quest(
			"IN_AID_OF_THE_MYREQUE", skill(CRAFTING, 25), skill(MINING, 15),
			skill(MAGIC, 7), IN_SEARCH_OF_THE_MYREQUE);
	public static final Quest DARKNESS_OF_HALLOWVALE = new Quest(
			"DARKNESS_OF_HALLOWVALE", skill(CONSTRUCTION, 5),
			skill(MINING, 20), skill(THIEVING, 22), skill(AGILITY, 26), skill(
					CRAFTING, 32), skill(MAGIC, 33), skill(STRENGTH, 40),
			IN_AID_OF_THE_MYREQUE);
	public static final Quest HUNT_FOR_RED_RAKTUBER = new Quest(
			"HUNT_FOR_RED_RAKTUBER", skills(45, HUNTER, CONSTRUCTION), skill(
					THIEVING, 38), COLD_WAR, SEA_SLUG);
	public static final Quest FORGETTABLE_TALE_OF_A_DRUNKEN_DWARF = new Quest(
			"FORGETTABLE_TALE_OF_A_DRUNKEN_DWARF", skill(COOKING, 22), skill(
					FARMING, 17), THE_GIANT_DWARF, FISHING_CONTEST);
	public static final Quest SPIRIT_OF_SUMMER = new Quest("SPIRIT_OF_SUMMER",
			skill(CONSTRUCTION, 40), skill(FARMING, 26), skill(PRAYER, 35),
			skill(SUMMONING, 19));
	public static final Quest TROLL_STRONGHOLD = new Quest("TROLL_STRONGHOLD",
			agility(15), DEATH_PLATEAU);
	public static final Quest EADGARS_RUSE = new Quest("EADGARS_RUSE",
			herblore(31), DRUIDIC_RITUAL, TROLL_STRONGHOLD);
	public static final Quest MY_ARMS_BIG_ADVENTURE = new Quest(
			"MY_ARMS_BIG_ADVENTURE", skill(WOODCUTTING, 10),
			skill(FARMING, 29), EADGARS_RUSE, THE_FUED, JUNGLE_POTION);
	public static final Quest BIG_CHOMPY_BIRD_HUNTING = new Quest(
			"BIG_CHOMPY_BIRD_HUNTING", skills(30, COOKING, RANGED), skill(
					FLETCHING, 5));
	public static final Quest ZOGRE_FLESH_EATERS = new Quest(
			"ZOGRE_FLESH_EATERS", skills(8, HERBLORE, STRENGTH), smithing(4),
			skill(FLETCHING, 30), BIG_CHOMPY_BIRD_HUNTING, JUNGLE_POTION);
	public static final Quest BETWEEN_A_ROCK = new Quest("BETWEEN_A_ROCK",
			skill(DEFENCE, 30), skill(MINING, 40), smithing(50), DWARF_CANNON,
			FISHING_CONTEST);
	public static final Quest DEVIOUS_MINDS = new Quest("DEVIOUS_MINDS",
			skills(50, RUNECRAFTING, FLETCHING), smithing(65), WANTED,
			TROLL_STRONGHOLD, RECRUITMENT_DRIVE, WHATS_MINE_IS_YOURS);
	public static final Quest ENAKHRAS_LAMENT = new Quest("ENAKHRAS_LAMENT",
			skill(CRAFTING, 50), skill(FIREMAKING, 45), skill(PRAYER, 43),
			skill(MAGIC, 39));
	public static final Quest LOST_CITY = new Quest("LOST_CITY", crafting(31),
			woodcutting(36));
	public static final Quest FAIRYTALE_1 = new FairyTale1();
	public static final Quest FAIRYTALE_2 = new Quest("FAIRYTALE_2",
			thieving(40), farming(49), herblore(57), FAIRYTALE_1);
	public static final Quest FAIRYTALE_3 = new Quest("FAIRYTALE_3", skill(
			MAGIC, 59), skill(FARMING, 54), skill(THIEVING, 51), skill(
			SUMMONING, 37), skill(CRAFTING, 36), FAIRYTALE_2, WOLF_WHISTLE);
	public static final Quest THE_FAMILY_CREST = new Quest("THE_FAMILY_CREST",
			skills(40, MINING, SMITHING, CRAFTING), skill(MAGIC, 59));
	public static final Quest FIGHT_ARENA = new Quest("FIGHT_ARENA");
	public static final Quest HAUNTED_MINE = new Quest("HAUNTED_MINE", skill(
			AGILITY, 15), skill(CRAFTING, 35), PRIEST_IN_PERIL);
	public static final Quest BARCRAWL = new Quest("BARCRAWL");
	public static final Quest HORROR_FROM_THE_DEEP = new Quest(
			"HORROR_FROM_THE_DEEP", skill(AGILITY, 35), BARCRAWL);
	public static final Quest IN_PYRE_NEED = new Quest("IN_PYRE_NEED", skill(
			FIREMAKING, 55), skill(FLETCHING, 53), skill(CRAFTING, 52));
	public static final Quest SOME_LIKE_IT_COLD = new Quest(
			"SOME_LIKE_IT_COLD", skills(50, CONSTRUCTION, THIEVING), skill(
					FISHING, 65), skill(CRAFTING, 56), HUNT_FOR_RED_RAKTUBER);
	public static final Quest TOKTZ_KET_DILL = new Quest("TOKTZ_KET_DILL",
			skill(ATTACK, 40), skill(MINING, 41), skill(CRAFTING, 43), skill(
					STRENGTH, 45), skill(CONSTRUCTION, 50));
	public static final Quest TEMPLE_OF_IKOV = new Quest("TEMPLE_OF_IKOV",
			thieving(42), ranged(40));
	public static final Quest DEFENDER_OF_VARROCK = new Quest(
			"DEFENDER_OF_VARROCK", agility(51), hunter(51), smithing(54),
			mining(59), SHIELD_OF_ARRAV, KNIGHTS_SWORD, ROMEO_AND_JULIET,
			DRAGON_SLAYER, TEMPLE_OF_IKOV, THE_FAMILY_CREST, WHAT_LIES_BELOW,
			GARDEN_OF_TRANQUILITY);
	public static final Quest SHILO_VILLAGE = new Quest("SHILO_VILLAGE",
			crafting(20), agility(32), JUNGLE_POTION);
	public static final Quest ONE_SMALL_FAVOUR = new Quest("ONE_SMALL_FAVOUR",
			agility(36), crafting(25), herblore(18), smithing(30),
			DRUIDIC_RITUAL, RUNE_MYSTERIES, SHILO_VILLAGE);
	public static final Quest KINGS_RANSOM = new Quest("KINGS_RANSOM",
			magic(45), defence(65), BLACK_KNIGHTS_FORTRESS, HOLY_GRAIL,
			MURDER_MYSTERY, ONE_SMALL_FAVOUR);
	public static final Quest LAND_OF_GOBLINS = new Quest("LAND_OF_GOBLINS",
			prayer(30), agility(36), fishing(36), thieving(36), herblore(37),
			ANOTHER_SLICE_OF_HAM, FISHING_CONTEST);
	public static final Quest THE_CHOSEN_COMMANDER = new Quest(
			"THE_CHOSEN_COMMANDER", agility(46), strength(46), thieving(46),
			LAND_OF_GOBLINS);
	public static final Quest HEROS_QUEST = new Quest("HEROS_QUEST",
			cooking(53), fishing(53), herblore(25), mining(50),
			questPoints(55), SHIELD_OF_ARRAV, LOST_CITY, DRAGON_SLAYER,
			MERLINS_CRYSTAL);
	public static final Quest THRONE_OF_MISCELLANIA = new Quest(
			"THRONE_OF_MISCELLANIA", THE_FREMMENIK_TRIALS, HEROS_QUEST);
	public static final Quest ROYAL_TROUBLE = new Quest("ROYAL_TROUBLE",
			agility(40), slayer(40), THRONE_OF_MISCELLANIA);
	public static final Quest THE_FREMMENIK_ISLES = new Quest(
			"THE_FREMMENIK_ISLES", construction(20), crafting(46), agility(40),
			woodcutting(56), THE_FREMMENIK_TRIALS);
	public static final Quest THE_PATH_OF_GLOUPHRIE = new Quest(
			"THE_PATH_OF_GLOUPHRIE", strength(60), thieving(56), slayer(56),
			ranged(47), agility(45), THE_EYES_OF_GLOUPHRIE, WATERFALL);
	public static final Quest LEGACY_OF_SEEGAZE = new Quest(
			"LEGACY_OF_SEEGAZE", construction(20), agility(29), slayer(31),
			mining(35), firemaking(40), crafting(47), magic(49),
			DARKNESS_OF_HALLOWVALE);
	public static final Quest UNDERGROUND_PASS = new Quest("UNDERGROUND_PASS",
			ranged(25), BIOHAZARD);
	public static final Quest RUM_DEAL = new Quest("RUM_DEAL", crafting(42),
			farming(40), fishing(50), prayer(47), slayer(42),
			ZOGRE_FLESH_EATERS);
	public static final Quest CABIN_FEVER = new Quest("CABIN_FEVER",
			agility(42), crafting(45), smithing(50), ranged(40),
			PIRATES_TREASURE, RUM_DEAL);
	public static final Quest RFD_PIRATE = new Quest("RFD_PIRATE", RFD_START,
			cooking(31));
	public static final Quest THE_GREAT_BRAIN_ROBBERY = new Quest(
			"THE_GREAT_BRAIN_ROBBERY", crafting(16), construction(30),
			prayer(50), CREATURE_OF_FENKENSTRAIN, CABIN_FEVER, RFD_PIRATE);
	public static final Quest SUMMERS_END = new Quest("SUMMERS_END",
			summoning(23), woodcutting(23), hunter(35), mining(45),
			firemaking(47), prayer(55), SPIRIT_OF_SUMMER);
	public static final Quest TROLL_ROMANCE = new Quest("TROLL_ROMANCE",
			agility(28), TROLL_STRONGHOLD);
	public static final Quest A_VOID_DANCE = new Quest("A_VOID_DANCE",
			hunter(46), construction(47), mining(47), summoning(48),
			herblore(49), woodcutting(52), thieving(54),
			QUIET_BEFORE_THE_SWARM, DRUIDIC_RITUAL, WOLF_WHISTLE);
	public static final Quest AS_A_FIRST_RESORT = new Quest(
			"AS_A_FIRST_RESORT", hunter(48), firemaking(51), woodcutting(58),
			BIG_CHOMPY_BIRD_HUNTING, ZOGRE_FLESH_EATERS);
	public static final Quest CONTACT = new Quest("CONTACT",
			ICTHLARINS_LITTLE_HELPER);
	public static final Quest DEALING_WITH_SCABARAS = new Quest(
			"DEALING_WITH_SCABARAS", firemaking(21), agility(50), thieving(60),
			strength(60), THE_FUED, ZOGRE_FLESH_EATERS, CONTACT);
	public static final Quest FORGIVENESS_OF_A_CHAOS_DWARF = new Quest(
			"FORGIVENESS_OF_A_CHAOS_DWARF", firemaking(61), hunter(61),
			strength(69), FORGETTABLE_TALE_OF_A_DRUNKEN_DWARF, BETWEEN_A_ROCK);
	public static final Quest ROCKING_OUT = new Quest("ROCKING_OUT",
			agility(60), thieving(63), crafting(66), smithing(69),
			THE_GREAT_BRAIN_ROBBERY);
	public static final Quest A_CLOCKWORK_SYRINGE = new Quest(
			"A_CLOCKWORK_SYRINGE", dg(50), slayer(61), construction(62),
			summoning(65), smithing(74), thieving(74), defence(76));
	public static final Quest REGICIDE = new Quest("REGICIDE", agility(56),
			UNDERGROUND_PASS);
	public static final Quest ROVING_ELVES = new Quest("ROVING_ELVES",
			REGICIDE, WATERFALL);
	public static final Quest MEP_1 = new Quest("MEP_1", ranged(60),
			thieving(50), BIG_CHOMPY_BIRD_HUNTING, ROVING_ELVES, SHEEP_HERDER);
	public static final Quest MEP_2 = new Quest("MEP_2", agility(60), MEP_1);
	public static final Quest WITHIN_THE_LIGHT = new Quest("WITHIN_THE_LIGHT",
			agility(69), woodcutting(75), fletching(70), ranged(75), MEP_2);
	public static final Quest START_LEGENDS_QUEST = new Quest(
			"START_LEGENDS_QUEST", questPoints(108), agility(50), crafting(50),
			herblore(45), magic(56), mining(52), prayer(43), smithing(50),
			strength(50), thieving(50), woodcutting(50), THE_FAMILY_CREST,
			HEROS_QUEST, SHILO_VILLAGE, UNDERGROUND_PASS, WATERFALL);
	public static final Quest LEGENDS_QUEST = new Quest("LEGENDS_QUEST",
			START_LEGENDS_QUEST);
	public static final Quest THE_BRANCHES_OF_DARKMEYER = new Quest(
			"THE_BRANCHES_OF_DARKMEYER", woodcutting(76), fletching(70),
			magic(70), slayer(67), crafting(64), farming(63), agility(63),
			LEGENDS_QUEST, LEGACY_OF_SEEGAZE);
	public static final Quest DESERT_TREASURE = new Quest("DESERT_TREASURE",
			slayer(10), firemaking(50), magic(50), thieving(53), DIGSITE,
			PRIEST_IN_PERIL, TEMPLE_OF_IKOV, THE_TOURIST_TRAP,
			TROLL_STRONGHOLD, WATERFALL);
	public static final Quest CURSE_OF_ARRAV = new Quest("CURSE_OF_ARRAV",
			slayer(37), summoning(41), agility(61), ranged(64), strength(64),
			mining(64), thieving(66), DEFENDER_OF_VARROCK, SHADES_OF_MORTON,
			TROLL_ROMANCE, TALE_OF_THE_MUSPAH, MISSING_MY_MUMMY);
	public static final Quest THE_TEMPLE_AT_SENNTISTEN = new Quest(
			"THE_TEMPLE_AT_SENNTISTEN", prayer(50), kudos(125),
			DESERT_TREASURE, DEVIOUS_MINDS, CURSE_OF_ARRAV);
	public static final Quest BACK_TO_MY_ROOTS = new Quest("BACK_TO_MY_ROOTS",
			agility(55), farming(53), slayer(59), woodcutting(72), FAIRYTALE_1,
			HAND_IN_THE_SAND, ONE_SMALL_FAVOUR, TRIBAL_TOTEM);
	public static final Quest PRISONER_OF_GLOUPHRIE = new Quest(
			"PRISONER_OF_GLOUPHRIE", agility(64), construction(60),
			runecrafting(61), thieving(66), THE_PATH_OF_GLOUPHRIE, WATERFALL);
	public static final Quest MONKEY_MADNESS = new Quest("MONKEY_MADNESS",
			THE_GRAND_TREE, TREE_GNOME_VILLAGE);
	public static final Quest GLORIOUS_MEMORIES = new Quest(
			"GLORIOUS_MEMORIES", agility(50), herblore(43), hunter(41),
			magic(57), MOUNTAIN_DAUGHTER, THE_FREMMENIK_ISLES, ROYAL_TROUBLE);
	public static final Quest LUNAR_DIPLOMACY = new Quest("LUNAR_DIPLOMACY",
			crafting(61), defence(40), firemaking(49), mining(60), herblore(5),
			runecrafting(14), magic(65), woodcutting(55), LOST_CITY,
			THE_FREMMENIK_TRIALS, RUNE_MYSTERIES, SHILO_VILLAGE);
	public static final Quest DREAM_MENTOR = new Quest("DREAM_MENTOR",
			combat(85), LUNAR_DIPLOMACY, EADGARS_RUSE);
	public static final Quest EASY_FREMMY_TASKS = new Quest(
			"EASY_FREMMY_TASKS", fishing(16), slayer(10));
	public static final Quest MED_FREMMY_TASKS = new Quest("MED_FREMMY_TASKS",
			hunter(55), cooking(48), thieving(42), crafting(33), defence(20),
			EASY_FREMMY_TASKS);
	public static final Quest HARD_FREMMY_TASKS = new Quest(
			"HARD_FREMMY_TASKS", fishing(55), woodcutting(54), crafting(52),
			firemaking(52), defence(50), strength(35), mining(60), magic(70),
			agility(35), construction(50), LUNAR_DIPLOMACY, ROYAL_TROUBLE,
			MED_FREMMY_TASKS);
	public static final Quest BLOOD_RUNS_DEEP = new Quest("BLOOD_RUNS_DEEP",
			attack(75), strength(75), slayer(65), DREAM_MENTOR,
			GLORIOUS_MEMORIES, HORROR_FROM_THE_DEEP, HARD_FREMMY_TASKS);
	public static final Quest SWAN_SONG = new Quest("SWAN_SONG",
			questPoints(100), magic(66), cooking(62), fishing(62),
			smithing(45), firemaking(42), crafting(40), ONE_SMALL_FAVOUR,
			GARDEN_OF_TRANQUILITY);
	public static final Quest RFD_SIR_AMIK = new Quest("RFD_SIR_AMIK",
			RFD_START, LOST_CITY, START_LEGENDS_QUEST);
	public static final Quest LOVE_STORY = new Quest("LOVE_STORY", magic(77),
			construction(68), smithing(68), crafting(67), SWAN_SONG,
			RFD_SIR_AMIK);
	public static final Quest KING_OF_THE_DWARVES = new Quest(
			"KING_OF_THE_DWARVES", mining(68), smithing(70), strength(77),
			FORGIVENESS_OF_A_CHAOS_DWARF, MY_ARMS_BIG_ADVENTURE);
	public static final Quest CATAPULT_CONSTRUCTION = new Quest(
			"CATAPULT_CONSTRUCTION", fletching(42), construction(44), REGICIDE);
	public static final Quest DEADLIEST_CATCH = new Quest("DEADLIEST_CATCH",
			hunter(67), thieving(70), fishing(70), TOWER_OF_LIFE,
			DRUIDIC_RITUAL);
	public static final Quest RFD_MONKEY = new Quest("RFD_MONKEY", cooking(70),
			agility(48), RFD_START, MONKEY_MADNESS);
	public static final Quest DO_NO_EVIL = new Quest("DO_NO_EVIL", ranged(50),
			construction(64), crafting(68), magic(70), thieving(70),
			ANIMAL_MAGNETISM, DEALING_WITH_SCABARAS, DESERT_TREASURE,
			MISSING_MY_MUMMY, RFD_MONKEY, SHADOW_OF_THE_STORM, SMOKING_KILLS);
	public static final Quest THE_ELDER_KILN = new Quest("THE_ELDER_KILN",
			magic(75), agility(60), mining(41));
	public static final Quest GRIM_TALES = new Quest("GRIM_TALES", farming(45),
			herblore(52), thieving(58), agility(59), woodcutting(71),
			WITCHS_HOUSE);
	public static final Quest THE_FIREMAKERS_CURSE = new Quest(
			"THE_FIREMAKERS_CURSE", firemaking(74), hitpoints(76), agility(64));
	public static final Quest THE_VOID_STARES_BACK = new Quest(
			"THE_VOID_STARES_BACK", magic(80), attack(78), strength(78),
			firemaking(71), construction(70), crafting(70), smithing(70),
			summoning(55), defence(10), A_VOID_DANCE);
	public static final Quest THE_BRINK_OF_EXTINCTION = new Quest(
			"THE_BRINK_OF_EXTINCTION", defence(80), smithing(80), mining(72),
			THE_ELDER_KILN);
	public static final Quest BIRTHRIGHT_OF_THE_DWARVES = new Quest(
			"BIRTHRIGHT_OF_THE_DWARVES", mining(80), smithing(82),
			strength(85), KING_OF_THE_DWARVES);
	public static final Quest NOMADS_REQUIEM = new Quest("NOMADS_REQUIEM",
			magic(75), prayer(70), mining(66), hunter(65), construction(60),
			KINGS_RANSOM);
	public static final Quest RFD_GOBLINS = new Quest("RFD_GOBLINS", RFD_START,
			GOBLIN_DIPLOMACY);
	public static final Quest RFD_DWARF = new Quest("RFD_DWARF", RFD_START,
			FISHING_CONTEST);
	public static final Quest RFD_DAVE = new Quest("RFD_DAVE", RFD_START,
			cooking(25), GERTUDES_CAT, SHADOW_OF_THE_STORM);
	public static final Quest RFD_LUMB_SAGE = new Quest("RFD_LUMB_SAGE",
			cooking(40), RFD_START, BIG_CHOMPY_BIRD_HUNTING, BIOHAZARD,
			DEMON_SLAYER, MURDER_MYSTERY, NATURE_SPIRIT, WITCHS_HOUSE);
	public static final Quest RFD_OGRE = new Quest("RFD_OGRE", cooking(41),
			firemaking(20), RFD_START, BIG_CHOMPY_BIRD_HUNTING);
	public static final Quest RFD_FULL = new RFD_Full();
	public static final Quest KILLED_BORK = new Quest("KILLED_BORK",
			WHAT_LIES_BELOW);
	public static final Quest IN_WARRIOR_GUILD = new Quest("IN_WARRIOR_GUILD");
	public static final Quest WHILE_GUTHIX_SLEEPS = new Quest(
			"WHILE_GUTHIX_SLEEPS", summoning(23), hunter(55), thieving(60),
			defence(65), farming(65), herblore(65), magic(75),
			questPoints(270), DEFENDER_OF_VARROCK, DREAM_MENTOR,
			HAND_IN_THE_SAND, KINGS_RANSOM, LEGENDS_QUEST, MEP_2,
			THE_PATH_OF_GLOUPHRIE, RFD_FULL, SUMMERS_END, SWAN_SONG,
			TEARS_OF_GUTHIX, ZOGRE_FLESH_EATERS, IN_WARRIOR_GUILD, KILLED_BORK);
	public static final Quest RITUAL_OF_THE_MAHJARRAT = new ROTM();
	public static final Quest THE_WORLD_WAKES = new Quest("THE_WORLD_WAKES",
			combat(100));
	public static final Quest FATE_OF_THE_GODS = new Quest("FATE_OF_THE_GODS",
			summoning(67), agility(73), divination(75), slayer(76), magic(79),
			MISSING_PRESUMED_DEATH);
	public static final Quest ONE_OF_A_KIND = new Quest("ONE_OF_A_KIND",
			magic(81), summoning(74), dg(67), divination(40),
			A_TAIL_OF_TWO_CATS, KINGS_RANSOM, THE_WORLD_WAKES,
			MISSING_PRESUMED_DEATH);
	public static final Quest PLAGUES_END = new PlaguesEnd();

}
