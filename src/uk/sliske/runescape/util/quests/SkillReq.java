package uk.sliske.runescape.util.quests;

import uk.sliske.runescape.util.quests.OtherReq.REQS;

public class SkillReq extends Requirement {

	public enum SKILL {
		ATTACK, DEFENCE, STRENGTH, CRAFTING, HITPOINTS, MAGIC, RANGED, MINING, SMITHING, PRAYER, DUNGEONEERING, DIVINATION, WOODCUTTING, FLETCHING, FIREMAKING, FISHING, COOKING, SUMMONING, FARMING, SLAYER, RUNECRAFTING, AGILITY, HERBLORE, THIEVING, HUNTER, CONSTRUCTION

		
	}

	public final SKILL skill;
	private final Skill level;

	public int level(){
		return level.getLevel();
	}
	public int xp(){
		return level.xp();
	}
	
	public SkillReq(SKILL skill, int level) {
		this(skill, level, !skillOrNot(skill));
	}

	private static boolean skillOrNot(SKILL skill) {
		switch (skill) {
//		case QUEST_POINTS:
//			return false;
//		case COMBAT_LVL:
//			return false;
//		case KUDOS:
//			return false;
		default:
			return true;
		}
	}

	private SkillReq(SKILL skill, int level, boolean b) {
		super(TYPE.SKILL);
		this.skill = skill;
		this.level = new Skill(level, b);
	}

	public static SkillReq attack(int lvl) {
		return new SkillReq(SKILL.ATTACK, lvl);
	}

	public static SkillReq defence(int lvl) {
		return new SkillReq(SKILL.DEFENCE, lvl);
	}

	public static SkillReq strength(int lvl) {
		return new SkillReq(SKILL.STRENGTH, lvl);
	}

	public static SkillReq crafting(int lvl) {
		return new SkillReq(SKILL.CRAFTING, lvl);
	}

	public static SkillReq hitpoints(int lvl) {
		return new SkillReq(SKILL.HITPOINTS, lvl);
	}

	// ////////////////////////////////////////
	public static SkillReq magic(int lvl) {
		return new SkillReq(SKILL.MAGIC, lvl);
	}

	public static SkillReq ranged(int lvl) {
		return new SkillReq(SKILL.RANGED, lvl);
	}

	public static SkillReq mining(int lvl) {
		return new SkillReq(SKILL.MINING, lvl);
	}

	public static SkillReq smithing(int lvl) {
		return new SkillReq(SKILL.SMITHING, lvl);
	}

	public static SkillReq prayer(int lvl) {
		return new SkillReq(SKILL.PRAYER, lvl);
	}

	// ///////////////////////////////////////
	public static SkillReq dg(int lvl) {
		return new SkillReq(SKILL.DUNGEONEERING, lvl);
	}

	public static SkillReq divination(int lvl) {
		return new SkillReq(SKILL.DIVINATION, lvl);
	}

	public static SkillReq woodcutting(int lvl) {
		return new SkillReq(SKILL.WOODCUTTING, lvl);
	}

	public static SkillReq fletching(int lvl) {
		return new SkillReq(SKILL.FLETCHING, lvl);
	}

	public static SkillReq firemaking(int lvl) {
		return new SkillReq(SKILL.FIREMAKING, lvl);
	}

	// ///////////////////////////////////////
	public static SkillReq fishing(int lvl) {
		return new SkillReq(SKILL.FISHING, lvl);
	}

	public static SkillReq cooking(int lvl) {
		return new SkillReq(SKILL.COOKING, lvl);
	}

	public static SkillReq summoning(int lvl) {
		return new SkillReq(SKILL.SUMMONING, lvl);
	}

	public static SkillReq farming(int lvl) {
		return new SkillReq(SKILL.FARMING, lvl);
	}

	public static SkillReq slayer(int lvl) {
		return new SkillReq(SKILL.SLAYER, lvl);
	}

	// ///////////////////////////////////////
	public static SkillReq runecrafting(int lvl) {
		return new SkillReq(SKILL.RUNECRAFTING, lvl);
	}

	public static SkillReq agility(int lvl) {
		return new SkillReq(SKILL.AGILITY, lvl);
	}

	public static SkillReq herblore(int lvl) {
		return new SkillReq(SKILL.HERBLORE, lvl);
	}

	public static SkillReq thieving(int lvl) {
		return new SkillReq(SKILL.THIEVING, lvl);
	}

	public static SkillReq hunter(int lvl) {
		return new SkillReq(SKILL.HUNTER, lvl);
	}

	// ///////////////////////////////////////
	public static SkillReq construction(int lvl) {
		return new SkillReq(SKILL.CONSTRUCTION, lvl);
	}

	public static OtherReq questPoints(int lvl) {
		return new OtherReq(REQS.QUEST_POINTS, lvl);
	}

	public static OtherReq combat(int lvl) {
		return new OtherReq(REQS.COMBAT_LVL, lvl);
	}

	public static OtherReq kudos(int lvl) {
		return new OtherReq(REQS.KUDOS, lvl);
	}
}
