package uk.sliske.runescape.util.quests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uk.sliske.runescape.util.quests.SkillReq.SKILL;

public class Planner {
	private final Quest goal;

	private List<Quest> quests;
	private List<Quest> completedQuests;

	private HashMap<SKILL, Skill> currentXPs;

	public Planner(Quest goal) {
		this.goal = goal;
		quests = goal.getFlattenedQuestReqsList();
		completedQuests = new ArrayList<>();
		currentXPs = new HashMap<>();
		quests.add(goal);

		while (completedQuests.size() < quests.size()) {
			boolean b = false;
			for (Quest q : quests) {
				if (completedQuests.contains(q))
					continue;
				if (hasReqs(q, currentXPs, completedQuests)) {
					currentXPs = q.xpWhenComplete(currentXPs);
					System.out.printf("Complete the quest %s\n", q.name);
					completedQuests.add(q);
					b = true;
				}
			}
			if (!b)
				for (Quest q : quests) {
					if (!completedQuests.contains(q)) {
						if (hasQuestReqs(q, completedQuests)) {
							List<SkillReq> sr = q.getSkillReqs();
							for (SkillReq s : sr) {
								int xp = 0;
								try {
									xp = currentXPs.get(s.skill).xp();
								} catch (Exception e) {
									
								}
								if (s.xp() > xp) {
									System.out.printf("Train %dxp in %s to reach level %d.\n", s.xp() - xp, s.skill, s.level());
									currentXPs.put(s.skill, new Skill(s.xp(), true));
								}
							}
							break;
						}
					}
				}
		}
	}

	private boolean hasReqs(Quest q, HashMap<SKILL, Skill> xps,
			List<Quest> quests) {
		if (hasQuestReqs(q, quests))
			return hasSkillReqs(q, xps);
		return false;
	}

	private boolean hasSkillReqs(Quest q, HashMap<SKILL, Skill> xps) {
		List<SkillReq> sreqs = q.getSkillReqs();
		for (SkillReq s : sreqs) {
			if (!xps.containsKey(s.skill))
				return false;
			if (s.xp() > xps.get(s.skill).xp())
				return false;
		}
		return true;
	}

	private boolean hasQuestReqs(Quest q, List<Quest> completedQuests) {
		List<Quest> qreqs = q.getFlattenedQuestReqsList();
		for (Quest q1 : qreqs) {
			if (!completedQuests.contains(q1))
				return false;
		}
		return true;
	}
}
