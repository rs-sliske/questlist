package uk.sliske.runescape.util.quests;

public abstract class Requirement {

	protected enum TYPE{
		SKILL,QUEST,OTHER
	}
	protected final TYPE type;
	
	protected Requirement(final TYPE type){
		this.type=type;
	}
	
}
