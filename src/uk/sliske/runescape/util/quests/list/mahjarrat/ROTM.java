package uk.sliske.runescape.util.quests.list.mahjarrat;

import static uk.sliske.runescape.util.quests.SkillReq.agility;
import static uk.sliske.runescape.util.quests.SkillReq.crafting;
import static uk.sliske.runescape.util.quests.SkillReq.mining;
import uk.sliske.runescape.util.quests.Quest;

public class ROTM extends Quest {

	public ROTM() {
		super("RITUAL_OF_THE_MAHJARRAT", crafting(76), agility(77), mining(76),
				ENAKHRAS_LAMENT, FAIRYTALE_3, HAZEEL_CULT, ROCKING_OUT,
				THE_SLUG_MENACE, A_TAIL_OF_TWO_CATS, THE_TEMPLE_AT_SENNTISTEN,
				WHILE_GUTHIX_SLEEPS);

	}

}
