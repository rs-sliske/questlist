package uk.sliske.runescape.util.quests.list.rfd;

import static uk.sliske.runescape.util.quests.SkillReq.questPoints;
import uk.sliske.runescape.util.quests.Quest;

public class RFD_Full extends Quest {

	public RFD_Full() {
		super("Recipe for Disastor - Killing Culineromancer", questPoints(176),
				RFD_DAVE, RFD_GOBLINS, RFD_MONKEY, RFD_LUMB_SAGE, RFD_DWARF,
				RFD_PIRATE, RFD_SIR_AMIK, RFD_OGRE, DESERT_TREASURE,
				HORROR_FROM_THE_DEEP);
	}

}
