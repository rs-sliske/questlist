package uk.sliske.runescape.util.quests.list.fairy;

import uk.sliske.runescape.util.quests.Quest;
import uk.sliske.runescape.util.quests.SkillReq.SKILL;

public class FairyTale1 extends Quest {
	public FairyTale1() {
		super("Fairytale 1 - Growing Pains", LOST_CITY,
		NATURE_SPIRIT, RESTLESS_GHOST);
		addXpReward(SKILL.FARMING, 3500);
		addXpReward(SKILL.ATTACK, 2000);
		addXpReward(SKILL.MAGIC, 1000);
	}
}
