package uk.sliske.runescape.util.quests;

public class OtherReq extends Requirement {

	public enum REQS {
		QUEST_POINTS, COMBAT_LVL, KUDOS
	}

	private REQS skill;
	private int level;

	public OtherReq(REQS skill, int level) {
		super(TYPE.OTHER);
		this.skill = skill;
		this.level = level;
	}

}
